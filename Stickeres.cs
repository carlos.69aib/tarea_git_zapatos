public class Stickers : Accesorio
{
    private readonly double _precio;

    public Stickers(Zapato zapato, double precio) : base(zapato)
    {
        this._precio = precio;
    }

    public override double CalcularValorConAccesorios()
    {
        return _precio;
    }

    public override string ToString()
    {
        return "\t"+"Stickers"+"\t\t\t"+_precio;
    }
}