public class PlantillasExtras : Accesorio
{
    private double _precio;

    public PlantillasExtras(Zapato zapato, double precio) : base(zapato)
    {
        this._precio = precio;
    }

    public override double CalcularValorConAccesorios()
    {
        return _precio;
    }

    public override string ToString()
    {
        return "\t"+"Plantillas Extras"+"\t\t\t"+_precio;
    }
}