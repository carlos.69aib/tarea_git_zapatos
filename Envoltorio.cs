public class EnvoltorioRegalo : Accesorio
{
    private double _precio;

    public EnvoltorioRegalo(Zapato zapato, double precio) : base(zapato)
    {
        this._precio = precio;
    }

    public override double CalcularValorConAccesorios()
    {
        return _precio;
    }

    public override string ToString()
    {
        return "\t"+"Envoltorio Regalo"+"\t\t"+_precio;
    }
}