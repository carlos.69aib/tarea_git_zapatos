public class CordonesExtras : Accesorio
{
    private double _precio;

    public CordonesExtras(Zapato zapato, double precio) : base(zapato)
    {
        this._precio = precio;
    }

    public override double CalcularValorConAccesorios()
    {
        return _precio;
    }

    public override string ToString()
    {
        return "\t"+"Cordones Extras"+"\t\t\t"+_precio;
    }
}
