public class ZapatoBase : Zapato
{
    public ZapatoBase(string id, string nombre, double valor, string talla) : base(id, nombre, valor, talla)
    {
    }

    public override double CalcularValorConAccesorios()
    {
        return Valor;
    }
}