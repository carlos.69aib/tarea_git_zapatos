internal class Program
{
    private static void Main(string[] args)
    {
        string correoDefinido = "carlos@correo.com";
        string contrasenaDefinida = "12345678";
        double dineroDisponible = 100.0;

        Console.WriteLine("Ingrese su correo electrónico:");
        string correoIngresado = Console.ReadLine();

        Console.WriteLine("Ingrese su contraseña:");
        string contrasenaIngresada = Console.ReadLine();

        Verificacion verificacion = new Verificacion(correoDefinido, contrasenaDefinida);

        if (verificacion.Verificar(correoIngresado, contrasenaIngresada))
        {
            Menu menu = new Menu();
            menu.MostrarMenu();

            Console.WriteLine("Seleccione el índice del zapato que desea agregar al carrito:");
            int indiceZapato = int.Parse(Console.ReadLine()) - 1;

            Carrito carrito = new Carrito();
            string zapatoSeleccionado = menu.ObtenerZapato(indiceZapato);
            carrito.AgregarAlCarrito(zapatoSeleccionado);

            double precioZapato = ObtenerPrecio(zapatoSeleccionado); 

            if (dineroDisponible >= precioZapato)
            {
                dineroDisponible -= precioZapato;
                Console.WriteLine("Compra realizada correctamente.");
                carrito.MostrarCarrito();
                Console.WriteLine("Dinero disponible o restante:");
                Console.WriteLine(dineroDisponible);
            }
            else
            {
                Console.WriteLine("Error: No posee suficiente dinero para comprar este artículo.");
            }
        }
        else
        {
            Console.WriteLine("Error: La cuenta de correo o la contraseña son incorrectas.");
        }

    }

        private static double ObtenerPrecio(string zapato)
    {
        
        string[] partes = zapato.Split('-');
        string precioTexto = partes[2].Trim();
        double precio = double.Parse(precioTexto.Substring(1));
        return precio;
    }
}

class Verificacion
{
    private string correo;
    private string contrasena;

    public Verificacion(string correo, string contrasena)
    {
        this.correo = correo;
        this.contrasena = contrasena;
    }

    public bool Verificar(string correoIngresado, string contrasenaIngresada)
    {
        return correo == correoIngresado && contrasena == contrasenaIngresada;
    }
}

class Menu
{
    private List<string> zapatos;

    public Menu()
    {
        zapatos = new List<string>
        {
            "Zapato 1 - Talla 39 - $50",
            "Zapato 2 - Talla 40 - $80",
            "Zapato 3 - Talla 41 - $100",
            "Zapato 4 - Talla 42 - $120"
        };
    }

    public void MostrarMenu()
    {
        Console.WriteLine("Seleccione un par de zapatos:");
        foreach (string zapato in zapatos)
        {
            Console.WriteLine(zapato);
        }
    }
    public string ObtenerZapato(int indice)
    {
        return zapatos[indice];
    }
}

using System.Collections.Generic;
class Carrito
{
    private List<string> seleccionados;

    public Carrito()
    {
        seleccionados = new List<string>();
    }

    public void AgregarAlCarrito(string zapato)
    {
        seleccionados.Add(zapato);
    }

    public void MostrarCarrito()
    {
        Console.WriteLine("Artículos en el carrito:");
        foreach (string seleccionado in seleccionados)
        {
            Console.WriteLine(seleccionado);
        }
    }
}
