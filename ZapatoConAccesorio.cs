public class ZapatoConAccesorios : Zapato
{
    private List<Zapato> _accesorios = new List<Zapato>();

    public ZapatoConAccesorios(Zapato zapatoBase, string talla) : base(zapatoBase.ID, zapatoBase.Nombre, zapatoBase.Valor, talla)
    {
    }

    public void AgregarAccesorio(Zapato accesorio)
    {
        this._accesorios.Add(accesorio);
    }

    public void QuitarAccesorio(Zapato accesorio)
    {
        this._accesorios.Remove(accesorio);
    }

    public override double CalcularValorConAccesorios()
    {
        double valorConAccesorios = Valor;
        foreach (var accesorio in _accesorios)
        {
            valorConAccesorios += accesorio.Valor;
        }
        return valorConAccesorios;
    }
}