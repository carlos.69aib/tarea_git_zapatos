public abstract class Accesorio : Zapato
{
    protected Zapato _zapato;

    public Accesorio(Zapato zapato) : base(zapato.ID, zapato.Nombre, zapato.Valor, zapato.Talla)
    {
        this._zapato = zapato;
    }

    public override double CalcularValorConAccesorios()
    {
        return _zapato.CalcularValorConAccesorios();
    }
}