public abstract class Zapato
{
    public string ID { get; set; }
    public string Nombre { get; set; }
    public double Valor { get; set; }
    public string Talla { get; set; }

    public Zapato(string id, string nombre, double valor, string talla)
    {
        this.ID = id;
        this.Nombre = nombre;
        this.Valor = valor;
        this.Talla = talla;
    }

    public abstract double CalcularValorConAccesorios();

    public override string ToString()
    {
        return ID+"\t"+Nombre+"\t"+Valor+"\t"+Talla;
    }
}