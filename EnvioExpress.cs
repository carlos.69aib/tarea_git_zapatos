public class EnvioExpress : Accesorio
{
    private double _precio;

    public EnvioExpress(Zapato zapato, double precio) : base(zapato)
    {
        this._precio = precio;
    }

    public override double CalcularValorConAccesorios()
    {
        return _precio;
    }

    public override string ToString()
    {
        return "\t"+"Envío Express"+"\t\t\t"+_precio;
    }
}