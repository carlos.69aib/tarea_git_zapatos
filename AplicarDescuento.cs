public class AplicarDescuento : Accesorio
{
    private double _descuentoPorcentaje;

    public AplicarDescuento(Zapato zapato, double descuentoPorcentaje) : base(zapato)
    {
        this._descuentoPorcentaje = descuentoPorcentaje;
    }

    public override double CalcularValorConAccesorios()
    {
        double valorConDescuento = _zapato.CalcularValorConAccesorios()-_zapato.CalcularValorConAccesorios();
        double descuento = _zapato.Valor * (_descuentoPorcentaje / 100);
        return valorConDescuento - descuento;
    }

    public double CalcularDescuento()
    {
        return _zapato.Valor * (_descuentoPorcentaje / 100);
    }

   
}