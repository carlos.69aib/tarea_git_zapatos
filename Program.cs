﻿using System;
using System.Collections.Generic;
internal class Program
{
    static List<Zapato> listaZapatos = new List<Zapato>();
    static Dictionary<string, string> clientesRegistrados = new Dictionary<string, string>();

    public static void Main(string[] args)
    {
        IniciarMenuPrincipal();
    }

    static void IniciarMenuPrincipal()
    {
        Console.WriteLine("1.- Iniciar como administrador");
        Console.WriteLine("2.- Iniciar como cliente");
        Console.WriteLine("3.- Registrarse como cliente");
        Console.WriteLine("4.- Finalizar Proceso");
        Console.Write("Seleccione el número de la opción que desea: ");

        string opcion = Console.ReadLine();

        switch (opcion)
        {
            case "1":
                IniciarComoAdministrador();
                break;
            case "2":
                IniciarComoCliente();
                break;
            case "3":
                RegistrarseComoCliente();
                break;
            case "4":
                Console.WriteLine("Finalizando el programa...");
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Opción no válida. Por favor, seleccione una opción válida.");
                IniciarMenuPrincipal();
                break;
        }
    }

    static void IniciarComoAdministrador()
    {
        Console.Write("Ingrese nombre de usuario: ");
        string usuario = Console.ReadLine();

        Console.Write("Ingrese contraseña: ");
        string contrasena = Console.ReadLine();

        if (EsUsuarioAdministradorValido(usuario, contrasena))
        {
            MostrarMenuAdministrador();
        }
        else
        {
            Console.WriteLine("Credenciales de administrador incorrectas.");
            IniciarMenuPrincipal();
        }
    }

    static bool EsUsuarioAdministradorValido(string usuario, string contrasena)
    {
        return usuario == "admin" && contrasena == "root";
    }

    static void MostrarMenuAdministrador()
    {
        Console.WriteLine("\nMenu de Administrador");
        Console.WriteLine("1.- Agregar Zapato");
        Console.WriteLine("2.- Quitar Zapato");
        Console.WriteLine("3.- Editar Detalles De Zapato");
        Console.WriteLine("4.- Atras");

        Console.Write("Seleccione el número de la opción que desea: ");
        string opcion = Console.ReadLine();

        switch (opcion)
        {
            case "1":
                AgregarZapato();
                MostrarMenuAdministrador();
                break;
            case "2":
                QuitarZapato();
                MostrarMenuAdministrador();
                break;
            case "3":
                EditarDetallesZapato();
                MostrarMenuAdministrador();
                break;
            case "4":
                IniciarMenuPrincipal();
                break;
            default:
                Console.WriteLine("Opción no válida. Por favor, seleccione una opción válida.");
                MostrarMenuAdministrador();
                break;
        }
    }

    static void AgregarZapato()
    {
        Console.WriteLine("\nZapatos disponibles:");

        if (listaZapatos.Count == 0)
        {
            Console.WriteLine("No hay zapatos en la lista.");
        }
        else
        {
            Console.WriteLine("ID\tNombre Zapatos\tValor\t\tTalla");
            foreach (var zapato in listaZapatos)
            {
                Console.WriteLine(zapato);
            }
        }

        Console.Write("\nID: ");
        string id = Console.ReadLine();

        Console.Write("Nombre Zapato: ");
        string nombreZapato = Console.ReadLine();

        Console.Write("Valor: ");
        if (!double.TryParse(Console.ReadLine(), out double valor))
        {
            Console.WriteLine("El valor ingresado no es válido.");
            return;
        }

        Console.Write("Talla: ");
        string talla = Console.ReadLine();

        if (ValidarZapato(id, nombreZapato, valor))
        {
            listaZapatos.Add(new ZapatoBase(id, nombreZapato, valor, talla));
            Console.WriteLine("El producto ha sido agregado con éxito.");
        }
        else
        {
            Console.WriteLine("No es válido agregar el producto.");
        }
    }

    static bool ValidarZapato(string id, string nombre, double valor)
    {
        if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(nombre) || valor <= 0)
            return false;

        if (listaZapatos.Exists(z => z.ID == id) || listaZapatos.Exists(z => z.Nombre == nombre))
            return false;

        return true;
    }

    static void QuitarZapato()
    {
        Console.WriteLine("\nZapatos disponibles:");

        if (listaZapatos.Count == 0)
        {
            Console.WriteLine("No hay zapatos en la lista.");
        }
        else
        {
            Console.WriteLine("ID\tNombre Zapatos\tValor\t\tTalla");
            foreach (var zapato in listaZapatos)
            {
                Console.WriteLine(zapato);
            }
        }

        Console.Write("\nIngrese la ID del producto para borrar: ");
        string id = Console.ReadLine();

        var zapatoEnLista = listaZapatos.Find(z => z.ID == id);

        if (zapatoEnLista != null)
        {
            listaZapatos.Remove(zapatoEnLista);
            Console.WriteLine("El producto ha sido borrado con éxito.");
        }
        else
        {
            Console.WriteLine("No es válido borrar el producto ya que no se encuentra en la lista.");
        }
    }

    static void EditarDetallesZapato()
    {
        Console.WriteLine("\nZapatos disponibles:");

        if (listaZapatos.Count == 0)
        {
            Console.WriteLine("No hay zapatos en la lista.");
        }
        else
        {
            Console.WriteLine("ID\tNombre Zapatos\tValor\t\tTalla");
            foreach (var zapato in listaZapatos)
            {
                Console.WriteLine(zapato);
            }
        }

        Console.Write("\nIngrese la ID del producto para editar: ");
        string id = Console.ReadLine();

        var zapatoEnLista = listaZapatos.Find(z => z.ID == id);

        if (zapatoEnLista != null)
        {
            Console.WriteLine("\nDetalles del producto:");

            Console.WriteLine($"ID:\t\t{zapatoEnLista.ID}");
            Console.WriteLine($"Nombre Zapato:\t{zapatoEnLista.Nombre}");
            Console.WriteLine($"Valor:\t\t{zapatoEnLista.Valor:C}");
            Console.WriteLine($"Talla:\t\t{zapatoEnLista.Talla}");

            Console.Write("\nIngrese el nuevo nombre del zapato: ");
            string nuevoNombre = Console.ReadLine();

            Console.Write("Ingrese el nuevo valor del zapato: ");
            if (!double.TryParse(Console.ReadLine(), out double nuevoValor))
            {
                Console.WriteLine("El valor ingresado no es válido.");
                return;
            }

            zapatoEnLista.Nombre = nuevoNombre;
            zapatoEnLista.Valor = nuevoValor;

            Console.WriteLine("El producto ha sido editado con éxito.");
        }
        else
        {
            Console.WriteLine("No es válido editar el producto ya que no se encuentra en la lista.");
        }
    }

    static void IniciarComoCliente()
    {
        Console.Write("Ingrese nombre de usuario: ");
        string usuario = Console.ReadLine();

        Console.Write("Ingrese contraseña: ");
        string contrasena = Console.ReadLine();

        if (EsClienteRegistrado(usuario, contrasena))
        {
            MostrarMenuCliente();
        }
        else
        {
            Console.WriteLine("Credenciales de cliente incorrectas.");
            IniciarMenuPrincipal();
        }
    }

    static bool EsClienteRegistrado(string usuario, string contrasena)
    {
        return clientesRegistrados.ContainsKey(usuario) && clientesRegistrados[usuario] == contrasena;
    }

    static void MostrarMenuCliente()
    {
        Console.WriteLine("\nMenu de Cliente");
        Console.WriteLine("1.- Agregar Zapato Al Carrito");
        Console.WriteLine("2.- Quitar Zapato Del Carrito");
        Console.WriteLine("3.- Calcular total");
        Console.WriteLine("4.- Atras");

        Console.Write("Seleccione el número de la opción que desea: ");
        string opcion = Console.ReadLine();

        switch (opcion)
        {
            case "1":
                AgregarZapatoAlCarrito();
                break;
            case "2":
                QuitarZapatoDelCarrito();
                break;
            case "3":
                CalcularTotal();
                break;
            case "4":
                IniciarMenuPrincipal();
                break;
            default:
                Console.WriteLine("Opción no válida. Por favor, seleccione una opción válida.");
                MostrarMenuCliente();
                break;
        }
    }

    static void AgregarZapatoAlCarrito()
    {
        Console.WriteLine("\nZapatos disponibles:");

        if (listaZapatos.Count == 0)
        {
            Console.WriteLine("No hay zapatos en la lista.");
            MostrarMenuCliente();
            return;
        }
        else
        {
            Console.WriteLine("ID\tNombre Zapatos\tValor\t\tTalla");
            foreach (var zapato in listaZapatos)
            {
                Console.WriteLine(zapato);
            }
        }

        Console.Write("\nIngrese el ID del producto que desea agregar al carrito: ");
        string id = Console.ReadLine();

        var zapatoEnLista = listaZapatos.Find(z => z.ID == id);

        if (zapatoEnLista != null)
        {
            Console.Write("Ingrese la talla del zapato: ");
            string talla = Console.ReadLine();

            if (ValidarTalla(talla))
            {
                carritoCompras.Add(new ZapatoConAccesorios(zapatoEnLista, talla));
                Console.WriteLine("El producto ha sido agregado al carrito con éxito.");

                MostrarMenuAccesorios(zapatoEnLista);
            }
            else
            {
                Console.WriteLine("Talla no válida. Debe ser un número entre 34 y 42.");
            }
        }
        else
        {
            Console.WriteLine("No es válido agregar el producto ya que no se encuentra en la lista.");
        }
    }

    static bool ValidarTalla(string talla)
    {
        if (int.TryParse(talla, out int tallaNum))
        {
            return tallaNum >= 34 && tallaNum <= 42;
        }
        return false;
    }

    static void MostrarMenuAccesorios(Zapato zapato)
    {
        Console.WriteLine("\n¿Desea agregar algún accesorio?");
        Console.WriteLine("1.- Aplicar Descuento");
        Console.WriteLine("2.- Envoltorio de Regalo");
        Console.WriteLine("3.- Stickers");
        Console.WriteLine("4.- Envío Express");
        Console.WriteLine("5.- Cordones Extras");
        Console.WriteLine("6.- Plantillas Extras");
        Console.WriteLine("7.- Finalizar Compra");

        Console.Write("Seleccione el número de la opción que desea: ");
        string opcion = Console.ReadLine();

        switch (opcion)
        {
            case "1":
                AgregarAccesorio(new AplicarDescuento(zapato, 5));
                MostrarMenuAccesorios(zapato);
                break;
            case "2":
                AgregarAccesorio(new EnvoltorioRegalo(zapato, 5));
                MostrarMenuAccesorios(zapato);
                break;
            case "3":
                AgregarAccesorio(new Stickers(zapato, 5));
                MostrarMenuAccesorios(zapato);
                break;
            case "4":
                AgregarAccesorio(new EnvioExpress(zapato, 10));
                MostrarMenuAccesorios(zapato);
                break;
            case "5":
                AgregarAccesorio(new CordonesExtras(zapato, 5));
                MostrarMenuAccesorios(zapato);
                break;
            case "6":
                AgregarAccesorio(new PlantillasExtras(zapato, 5));
                MostrarMenuAccesorios(zapato);
                break;
            case "7":
                MostrarMenuCliente();
                break;
            default:
                Console.WriteLine("Opción no válida. Por favor, seleccione una opción válida.");
                MostrarMenuAccesorios(zapato);
                break;
        }
    }

    static void AgregarAccesorio(Zapato accesorio)
    {
        carritoCompras.Add(accesorio);
        Console.WriteLine("El accesorio ha sido agregado con éxito.");
    }

    static List<Zapato> carritoCompras = new List<Zapato>();

    static void QuitarZapatoDelCarrito()
    {
        Console.WriteLine("\nZapatos en el carrito:");

        if (carritoCompras.Count == 0)
        {
            Console.WriteLine("No hay zapatos en el carrito.");
        }
        else
        {
            Console.WriteLine("ID\tNombre Zapatos\tValor\t\tTalla");
            foreach (var zapato in carritoCompras)
            {
                Console.WriteLine(zapato);
            }
        }

        Console.Write("\nIngrese la ID del producto para quitar del carrito: ");
        string id = Console.ReadLine();

        var zapatoEnCarrito = carritoCompras.Find(z => z.ID == id);

        if (zapatoEnCarrito != null)
        {
            carritoCompras.Remove(zapatoEnCarrito);
            Console.WriteLine("El producto ha sido quitado del carrito con éxito.");
        }
        else
        {
            Console.WriteLine("No es válido quitar el producto ya que no se encuentra en el carrito.");
        }
    }

    static void CalcularTotal()
    {
        Console.WriteLine("\nProductos en el carrito:");

        if (carritoCompras.Count == 0)
        {
            Console.WriteLine("No hay productos en el carrito.");
            MostrarMenuCliente();
            return;
        }
        else
        {
            Console.WriteLine("ID\tNombre Zapatos\tValor\t\tTalla");
            foreach (var zapato in carritoCompras)
            {
                Console.WriteLine(zapato);
            }
        }

        double subtotal = 0;
        double descuento = 0;

        foreach (var zapato in carritoCompras)
        {
            subtotal += zapato.CalcularValorConAccesorios();
            if (zapato is AplicarDescuento descuentoZapato)
            {
                descuento += descuentoZapato.CalcularDescuento();
            }
        }

        double iva = subtotal * 0.12;
        double total = subtotal + iva - descuento;

        Console.WriteLine("Subtotal"+"\t"+subtotal);
        Console.WriteLine("IVA"+"\t"+iva);
        Console.WriteLine("Descuento"+"\t"+descuento);
        Console.WriteLine("Total"+"\t"+total
        );

        MostrarMenuFactura();
    }

    static void MostrarMenuFactura()
    {
        Console.WriteLine("\n1.- Finalizar proceso de compra");
        Console.WriteLine("2.- Regresar al menu anterior");

        Console.Write("Seleccione el número de la opción que desea: ");
        string opcion = Console.ReadLine();

        switch (opcion)
        {
            case "1":
                Console.WriteLine("Finalizando el programa...");
                Environment.Exit(0);
                break;
            case "2":
                MostrarMenuCliente();
                break;
            default:
                Console.WriteLine("Opción no válida. Por favor, seleccione una opción válida.");
                MostrarMenuFactura();
                break;
        }
    }

    static void RegistrarseComoCliente()
    {
        Console.Write("Ingrese nombre de usuario con el que desea registrarse: ");
        string usuario = Console.ReadLine();

        Console.Write("Ingrese contraseña para registrarse: ");
        string contrasena = Console.ReadLine();

        Console.Write("Confirme contraseña para registrarse: ");
        string confirmarContrasena = Console.ReadLine();

        if (contrasena == confirmarContrasena)
        {
            clientesRegistrados[usuario] = contrasena;
            Console.WriteLine("Registro exitoso. Ahora puede iniciar sesión como cliente.");
            IniciarMenuPrincipal();
        }
        else
        {
            Console.WriteLine("Las contraseñas no coinciden. Inténtelo nuevamente.");
            RegistrarseComoCliente();
        }
    }
}

